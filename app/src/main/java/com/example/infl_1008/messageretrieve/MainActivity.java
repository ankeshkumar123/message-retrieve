package com.example.infl_1008.messageretrieve;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MAIN ACTIVITY";

    private Realm mRealm;

    String smsDate;
    RealmResults<RealmModel> mDatabaseResult;
    String body;
    RecyclerView mRecycleView;
    LinearLayoutManager mLayoutManager;
    MyAdapter myAdapter;
    MaterialSearchView inputSearch;

    Cursor cursor;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                RealmResults<RealmModel> searchResult = mRealm.where(RealmModel.class).beginsWith("name", newText, Case.INSENSITIVE).findAll();
                myAdapter = new MyAdapter(searchResult);
                mRecycleView.setAdapter(myAdapter);
                Log.d(TAG, "onQueryTextChange: ");
                return true;
            }
        });
        return true;

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        RealmConfiguration config = new RealmConfiguration
                .Builder(this)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);
        mRealm = Realm.getDefaultInstance();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search Here...");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);

        mRecycleView = (RecyclerView) findViewById(R.id.recycle_view);

        mDatabaseResult = mRealm.where(RealmModel.class).findAll();
        myAdapter = new MyAdapter(mDatabaseResult);
        mRecycleView.setAdapter(myAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecycleView.setLayoutManager(mLayoutManager);

        new BackgroundOperation().execute();
        SimpleTouchCallback callback = new SimpleTouchCallback(myAdapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(mRecycleView);


    }


    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> implements SwipeListener {
        private List<RealmModel> msgList;

        public MyAdapter(List<RealmModel> msgList) {
            this.msgList = msgList;
        }

        @Override
        public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycleview_child, parent, false);

            return new MyHolder(itemView);

        }

        @Override
        public void onBindViewHolder(MyHolder holder, int position) {
            RealmModel model = msgList.get(position);
            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd-MM-yyy");
            holder.date_text.setText(timeStampFormat.format(model.getDate()));
            holder.msg_txt.setText(model.getMessage());
            holder.name_txt.setText(model.getName());

        }


        @Override
        public int getItemCount() {
            return msgList.size();
        }

        @Override
        public void onSwipe(int position) {
            mRealm.beginTransaction();
            mDatabaseResult.get(position).removeFromRealm();
            mRealm.commitTransaction();
            notifyDataSetChanged();
        }

        public class MyHolder extends RecyclerView.ViewHolder {
            TextView date_text, name_txt, msg_txt;

            public MyHolder(View view) {
                super(view);
                date_text = (TextView) view.findViewById(R.id.date_txt);
                msg_txt = (TextView) view.findViewById(R.id.msg_txt);
                name_txt = (TextView) view.findViewById(R.id.name_txt);
            }
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    class BackgroundOperation extends AsyncTask<Object, Object, ArrayList<RealmModel>> {
        private static final String TAG = "ASYNC TASK";


        @Override
        protected ArrayList<RealmModel> doInBackground(Object... params) {
            return useCurser();
        }

        @Override
        protected void onPostExecute(ArrayList<RealmModel> cursorResult) {
            super.onPostExecute(cursorResult);
            // if database contains some messages inside it
            if (mDatabaseResult.size() > 0) {
                RealmModel dbLastMessage = mDatabaseResult.first();
                Log.d(TAG, "Database Last Message: " + dbLastMessage);
                RealmModel originalLastMessage = cursorResult.get(0);
                Log.d(TAG, "Original Last Message: " + originalLastMessage);
                RealmModel intersection = new RealmModel();
                for (RealmModel model : cursorResult) {
                    if (dbLastMessage.getMessage().equalsIgnoreCase(model.getMessage())) {
                        intersection = model;
                        Log.d(TAG, "Intersection Found");
                        break;
                    }else {
                        Log.d(TAG, "onPostExecute: Intersection Not Found ");
                    }
                }
                int onAddIndex = cursorResult.indexOf(intersection);
                Log.d(TAG, "Intersection: " + onAddIndex);

                if (!originalLastMessage.getMessage().equalsIgnoreCase(dbLastMessage.getMessage())) {

                    Toast.makeText(MainActivity.this, "Different Sizes", Toast.LENGTH_SHORT).show();
                    mRecycleView.invalidate();
                    myAdapter = new MyAdapter(cursorResult);
                    mRecycleView.setAdapter(myAdapter);
                    mRealm.beginTransaction();
                    while (onAddIndex >= 0) {
                        RealmModel m = cursorResult.get(--onAddIndex);
                        RealmModel model = mRealm.createObject(RealmModel.class);
                        model.setDate(m.getDate());
                        model.setMessage(m.getMessage());
                        model.setName(m.getName());
                    }
                    mRealm.commitTransaction();

                    Toast.makeText(MainActivity.this, "Message Updated", Toast.LENGTH_SHORT).show();


                } else {
               /* mRealm.beginTransaction();
                mRealm.deleteAll();
                mRealm.commitTransaction();*/
                    Toast.makeText(MainActivity.this, "Nothing to update", Toast.LENGTH_SHORT).show();
                }
            } else {

                myAdapter = new MyAdapter(cursorResult);
                mRecycleView.invalidate();
                mRecycleView.setAdapter(myAdapter);
                mRealm.beginTransaction();
                mRealm.deleteAll();
                for (RealmModel m : cursorResult) {
                    RealmModel model = mRealm.createObject(RealmModel.class);
                    model.setDate(m.getDate());
                    model.setMessage(m.getMessage());
                    model.setName(m.getName());
                }
                mRealm.commitTransaction();

                Toast.makeText(MainActivity.this, "Message Updated", Toast.LENGTH_SHORT).show();
            }

        }

        public ArrayList<RealmModel> useCurser() {

            ArrayList<RealmModel> bgResult = new ArrayList<>();
            Uri uriSms = Uri.parse("content://sms/inbox");
            Cursor crsr = getContentResolver().query(uriSms, new String[]{"_id", "address", "date", "body"}, null, null, null);
            assert crsr != null;
            crsr.moveToFirst();
            while (!crsr.isAfterLast()) {
            /*String address = cursor.getString(1);*/
                body = crsr.getString(3);

                String date = crsr.getString(2);

                Long timestamp = Long.parseLong(date);
                Date smsDate = new Date(timestamp);


                String name = readNumber(crsr.getString(1));
                if (name == null) {
                    name = crsr.getString(1);

                }


                RealmModel model = new RealmModel(smsDate, name, body);
                bgResult.add(model);
                crsr.moveToNext();
            }
            crsr.close();


            return bgResult;
        }


        public String readNumber(String number) {
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
            ContentResolver cr = getContentResolver();
            Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null, null);

            String contactName = null;
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            return contactName;
        }


    }

}
