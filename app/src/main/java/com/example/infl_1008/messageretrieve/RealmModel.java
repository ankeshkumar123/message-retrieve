package com.example.infl_1008.messageretrieve;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by Infl_1008 on 2/14/2017.
 */

public class RealmModel extends RealmObject
{
    @Required
    Date date;
    @Required
    String name;
    @Required
    String message;

    @Override
    public String toString() {
        return "RealmModel{" +
                "date='" + date + '\'' +
                ", name='" + name + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public RealmModel(Date date, String name, String message)
    {
        this.date = date;
        this.name = name;
        this.message = message;
    }

    public RealmModel()
    {
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
