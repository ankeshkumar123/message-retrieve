package com.example.infl_1008.messageretrieve;

/**
 * Created by Infl_1008 on 2/15/2017.
 */

public interface SwipeListener
{
    public void onSwipe(int position);
}
